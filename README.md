# Digital Scholars Hub resources

Here you will find resources related to R, Python, Bash, Git, OpenStreetMap, QGIS, OpenRefine and Audacity sessions held at the UQ Library.

Other training resources are also available [on the Library website](https://web.library.uq.edu.au/library-services/training/software-training-resources).

## Format

Most sessions use the "live-coding" format in which the instructor and the attendees type and execute commands together. A collaborative online pad is used to list useful links, challenges, and to interact during the session.

## Quick access to course resources

### R sessions

These R sessions are listed in the recommended order.

This is the Library's **regular R cycle**:

| Title | Course notes | Live collaborative pad | Video |
|:-|:-:|:-:|:-:|
| R with RStudio: getting started | [md](R/rstudio_intro/rstudio_intro.md) <sup>[source](R/rstudio_intro/rstudio_intro.Rmd) | [html](https://demo.codimd.org/s/rkfyJemYE) | [YT](https://www.youtube.com/watch?v=BoDTNbP7_OQ&list=PLmDEaZ20fWqCypV7S-trCPtVefHk4e0bU&index=2&t=0s), [LBRY](lbry://RwithRStudiogettingstarted#5) |
| R data manipulation with RStudio and dplyr: introduction | [md](R/dplyr/dplyr.md) <sup>[source](R/dplyr/dplyr.Rmd)</sup> | [html](https://demo.codimd.org/s/HyyLCm3KN) | [YT](https://www.youtube.com/watch?v=vqvsyaaqJUk&list=PLmDEaZ20fWqCypV7S-trCPtVefHk4e0bU&index=2) |
| R data visualisation with RStudio and ggplot2: introduction | [md](R/ggplot2_intro/ggplot2_intro.md) <sup>[source](R/ggplot2_intro/ggplot2_intro.Rmd)</sup> | [html](https://demo.codimd.org/s/rJIPr0vi4) | [YT](https://www.youtube.com/watch?v=LoeBgXSJWTw&list=PLmDEaZ20fWqCypV7S-trCPtVefHk4e0bU&index=3) |
| R data visualisation with RStudio and ggplot2: intermediate | [md](R/ggplot2_intermediate/ggplot2_intermediate.md) <sup>[source](R/ggplot2_intermediate/ggplot2_intermediate.Rmd)</sup> | [html](https://demo.codimd.org/s/rJLdcW-24) | [YT](https://www.youtube.com/watch?v=zzXCkYR84M0&list=PLmDEaZ20fWqCypV7S-trCPtVefHk4e0bU&index=4) |
| R and the Tidyverse: next steps | [md](R/tidyverse_next_steps/tidyverse_next_steps.md) <sup>[source](R/tidyverse_next_steps/tidyverse_next_steps.Rmd)</sup> | [html](https://demo.codimd.org/s/BkQCcmiOV) | [YT](https://www.youtube.com/watch?v=2TZYeFcJQIk&list=PLmDEaZ20fWqCypV7S-trCPtVefHk4e0bU&index=6) |
| R reproducible reports with R Markdown and knitr | [md](R/reports/reports.md) <sup>[source](R/reports/reports.Rmd)</sup> | [html](https://demo.codimd.org/s/S1Aka1waI) | ... |

We also offer these more specialised specialised (run less regularly, but feel free to ask us to schedule one of them):

| Title | Course notes | Live collaborative pad | Video |
|:-|:-:|:-:|:-:|
| R data visualisation with RStudio: heatmaps | [md](R/heatmaps/heatmaps_intermediate.md) <sup>[source](R/heatmaps/heatmaps_intermediate.Rmd)</sup> | [html](https://etherpad.wikimedia.org/p/cds-heatmaps) | [YT](https://www.youtube.com/watch?v=V-IRkO4NIHU&list=PLmDEaZ20fWqCypV7S-trCPtVefHk4e0bU&index=5) |
| R advanced: packaging and sharing functions | [md](R/packaging/packaging.md) <sup>[source](R/packaging/packaging.Rmd)</sup> | [html](https://demo.codimd.org/s/ryCzbvgXB) | ... |
| R advanced: webapps with Shiny | [md](R/shiny/shiny.md) <sup>[source](R/shiny/shiny.Rmd)</sup> | [html](https://demo.codimd.org/s/S19FIXxg8) | ... |

### Other tools

| Title | Course notes | Live collaborative pad | Video |
|:-|:-:|:-:|:-:|
| Unix Shell: an introduction | [md](Shell/shell_intro.md) | [html](https://etherpad.wikimedia.org/p/cds-shell) | ... |
| Git version control for collaboration | [md](Git/git.md) | [html](https://etherpad.wikimedia.org/p/cds-git) | ... |
| OpenRefine: introduction to dealing with messy data | [md](OpenRefine/openrefine.md) | [html](https://demo.codimd.org/s/rJCXmqviH) | ... |
| Python with Spyder: an introduction to data science | [md](Python/python_intro.md) | [html](https://etherpad.wikimedia.org/p/cds-python) | ... |
| OpenStreetMap: contribute and use the data | [md](OSM/OpenStreetMap.md) | [html](https://cryptpad.fr/pad/#/2/pad/edit/ZwpBgdie-YBBXvDE68N53u2s/) | ... |
| QGIS: introduction to mapping | [md](QGIS/intro/QGIS_intro.md) | [html](https://demo.codimd.org/s/B156o7z2V) | ... |
| QGIS: raster analysis | [md](QGIS/raster/QGIS_raster.md) | [html](https://demo.codimd.org/s/ryN5E-XuS) | ... |
| Audacity: introduction to audio editing_ | [md](Audacity/audacity.md) | [html](https://demo.codimd.org/s/HkkITjw9L) | ... |
| _next: Open Source tools for your research_ | ... | ... | ... |
| _next: Python data transformation_ | ... | ... | ... |
| _next: Wikipedia: start contributing_ | ... | ... | ... |

### One-off sessions

Some sessions are not run regularly, but files can also be hosted here.

| Title | Course notes | Live collaborative pad |
|:-|:-:|:-:|
| Introduction to scientific programming | [md](intro_to_programming/intro_to_programming.md) <sup>[source](intro_to_programming/intro_to_programming.Rmd)</sup> | [html](https://cryptpad.fr/code/#/2/code/edit/Op8PvBdGbBxBO9efXUuEYGlB/) |

### Files hosted here

Files available for each course, hosted in this repository:

* **Markdown notes** that the instructor can use to teach, and that students can refer to after the course;
* A **HTML pad export** that can be used to initialise a collaborative pad (https://etherpad.wikimedia.org, https://cryptpad.fr/code and https://demo.codimd.org/ are recommended to publish).

## Licence

All of the information on this repository (https://gitlab.com/stragu/DSH/) is freely available under the [Creative Commons - Attribution 4.0 International Licence](https://creativecommons.org/licenses/by/4.0/). You may re-use and re-mix the material in any way you wish, without asking permission, provided you cite the original source. However, we'd love to hear about what you do with it!

Part of this repository is based on [Paula Andrea Martinez](https://orcid.org/0000-0002-8990-1985)'s work [available under the same CC-By-4.0 licence](https://github.com/orchid00/CDS).

## Contributing

If you have questions about contributing to the material, please contact the DSH. You can also raise an issue or create a pull request if you spot something odd. If you would like to develop on top of this, please cite the source as mentioned above.

## Contact

If you are part of the UQ community, you can contact the library trainers for a 1-on-1 consultation, an enquiry about sessions, or any question about the programs supported by the UQ Library: training<sub><[commercial at](https://en.wikipedia.org/wiki/At_sign)></sub>library.uq.edu.au
